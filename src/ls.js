"use strict";

var fs = require('fs'),
    _ = require('lodash'),
    path = require('path'),
    log = function(e) { console.log(e); };

fs.readdir(process.argv[2], function(err, files) {
    if(err)
        console.error("fatal : " + err);
    else {
        var regular_files = [],
            directories = [],
            processed = 0;

        var done = function() {
            if(regular_files.length) {
                console.log('Files:');
                _(regular_files)
                    .map(function(file) { return '    ' + file; })
                    .each(log);
            }
            if(directories.length) {
                console.log('Directories:');
                _(directories)
                    .map(function(dir) { return '    ' + dir; })
                    .each(log);
            }
        };

        _.each(files, function(file) {
            fs.stat(path.join(process.argv[2],file), function(err, stat) {
                if(err)
                    console.error('fatal ' + err);
                else if(stat.isFile())
                    regular_files.push(file);
                else if(stat.isDirectory())
                    directories.push(file);

                ++processed;
                if(processed === files.length)
                    done();
            });
        });
    }
});
