var gulp = require('gulp');
var traceur = require('gulp-traceur');
var watch = require('gulp-watch');

gulp.task('default', function() {
  return gulp.src('src/*.js')
    .pipe(watch())
    .pipe(traceur({
      blockBinding: 'parse',
      arrowFunctions: true,
      generators: 'parse',
      forOf : 'parse'
    }))
    .pipe(gulp.dest('build')) ;
});
